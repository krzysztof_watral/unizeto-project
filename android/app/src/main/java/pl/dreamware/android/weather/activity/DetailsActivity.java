package pl.dreamware.android.weather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import pl.dreamware.android.weather.R;
import pl.dreamware.android.weather.ServiceProvider;
import pl.dreamware.android.weather.Utils;
import pl.dreamware.android.weather.adapter.ForecastAdapter;
import pl.dreamware.android.weather.feed.WeatherApiService;
import pl.dreamware.android.weather.model.Forecast;
import pl.dreamware.android.weather.model.Location;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {

    private static final String TAG = DetailsActivity.class.getSimpleName();

    public static final String EXTRA_EVENT = "extra_location";
    public static final String EXTRA_POSITION = "extra_position";
    public static final int LOCATION_REMOVED = -19;

    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Location location = getIntent().getExtras().getParcelable(EXTRA_EVENT);
        position = getIntent().getExtras().getInt(EXTRA_POSITION);

        setTitle(location.getPlace());

        final ListView forecastListView = (ListView) findViewById(R.id.forecastListView);
        final ForecastAdapter forecastAdapter = new ForecastAdapter(this, 0);

        WeatherApiService service = ServiceProvider.forGson(getString(R.string.weather_feed_uri));
        Call<List<Forecast>> forecastCall = service.getForecast(location.getId());

        Log.i(TAG, "request to: " + getString(R.string.weather_feed_uri));
        forecastCall.enqueue(new Callback<List<Forecast>>() {
            @Override
            public void onResponse(Call<List<Forecast>> call, Response<List<Forecast>> response) {
                if (Utils.isHttpOk(TAG, response) && Utils.isNotEmpty(response.body())) {
                    for (Forecast forecast : response.body()) {
                        forecastAdapter.add(forecast);
                    }
                    forecastAdapter.notifyDataSetChanged();
                    forecastListView.setAdapter(forecastAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<Forecast>> call, Throwable t) {
                Log.e(TAG, "Cannot get forecast list for location: {" + location.getId() + "} " + location.getPlace(), t);
            }
        });

        Button remove = (Button) findViewById(R.id.remove);
        assert remove != null;
        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String uri = view.getContext().getString(R.string.weather_feed_uri);
                WeatherApiService service = ServiceProvider.forScalars(uri);
                Call<Void> voidCall = service.removeLocation(location.getId());
                voidCall.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        if (Utils.isHttpOk(TAG, response)) {
                            Intent intent = new Intent();
                            intent.putExtra(EXTRA_POSITION, position);
                            setResult(LOCATION_REMOVED, intent);
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Log.e(TAG, "Cannot remove location", t);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_POSITION, position);
        setResult(RESULT_OK, intent);
        finish();
        return true;
    }
}