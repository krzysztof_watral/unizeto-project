package pl.dreamware.android.weather;

import android.util.Log;

import java.net.HttpURLConnection;
import java.util.Collection;

import retrofit2.Response;

public final class Utils {
    private Utils() {
        // utility class
    }

    static public boolean isHttpOk(String TAG, Response<?> response) {
        if (response.code() != HttpURLConnection.HTTP_OK) {
            Log.w(TAG, response.code() + " -> " + response.message());
            return false;
        }
        return true;
    }

    static public boolean isNotEmpty(Collection<?> list) {
        return list != null && !list.isEmpty();
    }
}