package pl.dreamware.android.weather.adapter;


import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;

import pl.dreamware.android.weather.R;
import pl.dreamware.android.weather.model.Forecast;

public class ForecastAdapter extends ArrayAdapter<Forecast> {
    public ForecastAdapter(Context context, int resource) {
        super(context, resource);
    }

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.0", DecimalFormatSymbols.getInstance());
    private static final DecimalFormat DECIMAL_FORMAT_INT = new DecimalFormat("0", DecimalFormatSymbols.getInstance());

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Forecast forecast = getItem(position);

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_forecast_list_item, parent, false);

            holder = new ViewHolder();
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.precipIntensity = (TextView) convertView.findViewById(R.id.precip_intensity);
            holder.temperature = (TextView) convertView.findViewById(R.id.temperature);
            holder.dewPoint = (TextView) convertView.findViewById(R.id.dew_point);
            holder.humidity = (TextView) convertView.findViewById(R.id.humidity);
            holder.windSpeed = (TextView) convertView.findViewById(R.id.wind_speed);
            holder.pressure = (TextView) convertView.findViewById(R.id.pressure);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.time.setText(DATE_FORMAT.format(forecast.getTime()));
        holder.precipIntensity.setText(DECIMAL_FORMAT.format(forecast.getPrecipIntensity()) + " mm");
        holder.temperature.setText(DECIMAL_FORMAT.format(forecast.getTemperature()) + " C");
        holder.dewPoint.setText(DECIMAL_FORMAT.format(forecast.getDewPoint()) + " C");
        holder.humidity.setText(DECIMAL_FORMAT_INT.format(forecast.getHumidity()) + " %");
        holder.windSpeed.setText(DECIMAL_FORMAT.format(forecast.getWindSpeed()) + " km/h");
        holder.pressure.setText(DECIMAL_FORMAT_INT.format(forecast.getPressure())+ "hPa");

        return convertView;
    }

    private class ViewHolder {
        private TextView time;

        private TextView precipIntensity;

        private TextView temperature;

        private TextView dewPoint;

        private TextView humidity;

        private TextView windSpeed;

        private TextView pressure;
    }
}
