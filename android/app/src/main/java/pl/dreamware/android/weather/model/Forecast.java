package pl.dreamware.android.weather.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class Forecast implements Parcelable{

    private Long time;

    private Double precipIntensity;

    private Double temperature;

    private Double dewPoint;

    private Double humidity;

    private Double windSpeed;

    private Double pressure;

    protected Forecast(Parcel in) {
        time = (Long) in.readSerializable();
        precipIntensity = in.readDouble();
        temperature = in.readDouble();
        dewPoint = in.readDouble();
        humidity = in.readDouble();
        windSpeed = in.readDouble();
        pressure = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(time);
        dest.writeDouble(precipIntensity);
        dest.writeDouble(temperature);
        dest.writeDouble(dewPoint);
        dest.writeDouble(humidity);
        dest.writeDouble(windSpeed);
        dest.writeDouble(pressure);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Forecast> CREATOR = new Creator<Forecast>() {
        @Override
        public Forecast createFromParcel(Parcel in) {
            return new Forecast(in);
        }

        @Override
        public Forecast[] newArray(int size) {
            return new Forecast[size];
        }
    };

    public Date getTime() {
        return new Date(time);
    }

    public Double getPrecipIntensity() {
        return precipIntensity;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getDewPoint() {
        return dewPoint;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public Double getPressure() {
        return pressure;
    }
}
