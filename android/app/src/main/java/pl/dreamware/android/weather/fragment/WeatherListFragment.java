package pl.dreamware.android.weather.fragment;

import android.app.Activity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import pl.dreamware.android.weather.R;
import pl.dreamware.android.weather.ServiceProvider;
import pl.dreamware.android.weather.Utils;
import pl.dreamware.android.weather.activity.DetailsActivity;
import pl.dreamware.android.weather.activity.MainActivity;
import pl.dreamware.android.weather.adapter.LocationAdapter;
import pl.dreamware.android.weather.feed.WeatherApiService;
import pl.dreamware.android.weather.model.Location;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherListFragment extends ListFragment {

    private static final String TAG = WeatherListFragment.class.getSimpleName();

    public static final int REQUEST_CODE_FORECAST = 1;

    public static WeatherListFragment getInstace() {
        return new WeatherListFragment();
    }

    private LocationAdapter locationAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setListShown(false);
        locationAdapter = new LocationAdapter(getActivity(), 0);

        WeatherApiService service = ServiceProvider.forGson(getString(R.string.weather_feed_uri));
        Call<List<Location>> locations = service.getLocations();

        Log.i(TAG, "request to: " + getString(R.string.weather_feed_uri));
        locations.enqueue(new Callback<List<Location>>() {
            @Override
            public void onResponse(Call<List<Location>> call, Response<List<Location>> response) {
                if (Utils.isHttpOk(TAG, response) && Utils.isNotEmpty(response.body())) {
                    for (Location location : response.body()) {
                        locationAdapter.add(location);
                        Log.i(TAG, location.getPlace());
                    }
                    locationAdapter.notifyDataSetChanged();
                    setListAdapter(locationAdapter);
                }
                setListShown(true);
            }

            @Override
            public void onFailure(Call<List<Location>> call, Throwable t) {
                Log.e(TAG, "Cannot get locations", t);
                setListShown(true);
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent(getActivity(), DetailsActivity.class);
        intent.putExtra(DetailsActivity.EXTRA_EVENT, locationAdapter.getItem(position));
        intent.putExtra(DetailsActivity.EXTRA_POSITION, position);
        startActivityForResult(intent, REQUEST_CODE_FORECAST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (REQUEST_CODE_FORECAST == requestCode) {
            if (Activity.RESULT_OK == resultCode) {
                setSelection(data.getIntExtra(DetailsActivity.EXTRA_POSITION, -1));
            } else if (DetailsActivity.LOCATION_REMOVED == resultCode) {
                ((MainActivity) getActivity()).displayInitialFragment();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
