package pl.dreamware.android.weather.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.lang3.StringUtils;

import pl.dreamware.android.weather.R;
import pl.dreamware.android.weather.ServiceProvider;
import pl.dreamware.android.weather.Utils;
import pl.dreamware.android.weather.feed.WeatherApiService;
import pl.dreamware.android.weather.fragment.WeatherListFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        handleDefineButtonClick();
        handleRefreshButtonClick();

        displayInitialFragment();
    }

    public void displayInitialFragment() {
        getFragmentManager().beginTransaction().replace(R.id.content_frame, WeatherListFragment.getInstace()).commit();
    }

    private void handleRefreshButtonClick() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayInitialFragment();
            }
        });
    }

    private void handleDefineButtonClick() {
        Button button = (Button) findViewById(R.id.search_button);
        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText editTextPhrase = (EditText) findViewById(R.id.search_phrase);
                String phrase = editTextPhrase.getText().toString();
                if (StringUtils.isNotBlank(phrase)) {
                    WeatherApiService service = ServiceProvider.forScalars(getString(R.string.weather_feed_uri));
                    Call<Boolean> res = service.defineLocation(phrase);
                    res.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            if (Utils.isHttpOk(TAG, response)) {
                                if (Boolean.TRUE.equals(response.body())) {
                                    displayInitialFragment();
                                    editTextPhrase.setText(StringUtils.EMPTY);
                                } else {
                                    Toast.makeText(getBaseContext(), "Please defined place precisely!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {
                            Log.e(TAG, "Cannot define location", t);
                        }
                    });

                } else {
                    Toast.makeText(v.getContext(), "Please provide text!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
