package pl.dreamware.android.weather;


import pl.dreamware.android.weather.feed.WeatherApiService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public final class ServiceProvider {
    private ServiceProvider() {
        //utility class
    }

    public static WeatherApiService forGson(String uri) {
        return new Retrofit.Builder()
                .baseUrl(uri)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WeatherApiService.class);
    }

    public static WeatherApiService forScalars(String uri) {
        return new Retrofit.Builder()
                .baseUrl(uri)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(WeatherApiService.class);
    }
}
