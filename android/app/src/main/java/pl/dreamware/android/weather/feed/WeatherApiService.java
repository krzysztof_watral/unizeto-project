package pl.dreamware.android.weather.feed;


import java.util.List;

import pl.dreamware.android.weather.model.Forecast;
import pl.dreamware.android.weather.model.Location;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WeatherApiService {

    @POST("/api/locations")
    Call<Boolean> defineLocation(@Body String place);

    @GET("/api/locations")
    Call<List<Location>> getLocations();

    @DELETE("/api/locations/{id}")
    Call<Void> removeLocation(@Path("id") Long id);

    @GET("/api/locations/{id}")
    Call<List<Forecast>> getForecast(@Path("id") Long id);
}
