package pl.dreamware.android.weather.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import pl.dreamware.android.weather.R;
import pl.dreamware.android.weather.ServiceProvider;
import pl.dreamware.android.weather.Utils;
import pl.dreamware.android.weather.activity.MainActivity;
import pl.dreamware.android.weather.feed.WeatherApiService;
import pl.dreamware.android.weather.model.Location;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationAdapter extends ArrayAdapter<Location> {

    private static final String TAG = LocationAdapter.class.getSimpleName();

    public LocationAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Location location = getItem(position);
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_location_list_item, parent, false);

            holder = new ViewHolder();
            holder.place = (TextView) convertView.findViewById(R.id.place);
            holder.latitude = (TextView) convertView.findViewById(R.id.latitude);
            holder.longitude = (TextView) convertView.findViewById(R.id.longitude);
//            holder.id = (Button) convertView.findViewById(R.id.id);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.place.setText(location.getPlace());
        holder.latitude.setText("lat: " + String.valueOf(location.getLatitude()));
        holder.longitude.setText("lon: " + String.valueOf(location.getLongitude()));

        return convertView;
    }

    private class ViewHolder {
        private Button id;
        private TextView place;
        private TextView latitude;
        private TextView longitude;
    }
}
