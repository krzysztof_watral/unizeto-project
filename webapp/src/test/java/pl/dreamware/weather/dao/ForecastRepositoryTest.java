package pl.dreamware.weather.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.dreamware.weather.Application;
import pl.dreamware.weather.entity.Location;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.notNullValue;

/**
 * example of integration test
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class ForecastRepositoryTest {

    @Autowired
    LocationRepository locationRepository;

    @Test
    public void getLocations() {
        List<Location> locations = locationRepository.findAll();
        assertThat(locations, notNullValue());
        assertThat(locations.size(), greaterThan(0));
    }

}
