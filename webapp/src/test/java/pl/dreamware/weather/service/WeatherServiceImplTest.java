package pl.dreamware.weather.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import pl.dreamware.weather.api.base.service.LocationDataFetcher;
import pl.dreamware.weather.cron.HistoryWeatherAsync;
import pl.dreamware.weather.dao.ForecastRepository;
import pl.dreamware.weather.dao.LocationRepository;
import pl.dreamware.weather.dao.WeatherRepository;
import pl.dreamware.weather.entity.Location;

/**
 * example of unit test
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceImplTest {

	@Mock
	private LocationRepository locationRepository;

	@InjectMocks
	WeatherServiceImpl service;

	@Mock
	private WeatherRepository weatherRepository;

	@Mock
	private ForecastRepository forecastRepository;

	@Mock
	private LocationDataFetcher locationDataFetcher;

	@Mock
	private HistoryWeatherAsync historyWeatherAsync;

	@Test
	public void definaLocationTestFalse() {
		when(locationDataFetcher.tryToGetLocation("location")).thenReturn(null);
		boolean result = service.defineLocation("location");
		assertEquals(result, false);
	}

	@Test
	public void definaLocationTestTrue() {
		Location location = Mockito.mock(Location.class);
		when(locationDataFetcher.tryToGetLocation("location")).thenReturn(location);
		boolean result = service.defineLocation("location");
		assertEquals(result, true);
	}

	@Test
	public void findAllLocationsTest() {
		List<Location> col = IntStream.range(0, 5)
				.mapToObj(i -> Mockito.mock(Location.class))
				.collect(Collectors.toList());

		when(locationRepository.findAll()).thenReturn(col);

		List<Location> result = service.getAllLocations();
		assertNotNull(result);
		assertEquals(result.size(), 5);
	}

}
