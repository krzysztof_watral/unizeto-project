package pl.dreamware.weather.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.dreamware.weather.entity.Location;

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {

}
