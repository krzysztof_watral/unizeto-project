package pl.dreamware.weather.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;

@Repository
public interface WeatherRepository extends JpaRepository<Weather, Long> {

    Weather findByLocationAndSourceService(Location location, SourceService sourceService);

    void removeByLocationId(long location);


}
