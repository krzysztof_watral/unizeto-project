package pl.dreamware.weather.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.mvc.model.AverageForecastModel;

import java.util.Date;
import java.util.List;

@Repository
public interface ForecastRepository extends JpaRepository<Forecast, Long> {

    @Query("select f from Forecast f left join f.weather w where w.sourceService = :service and w.location = :location")
    List<Forecast> find(@Param("service") SourceService sourceService, @Param("location") Location location);

    Long removeByTimeBefore(Date date);

    @Query(value = "SELECT new pl.dreamware.weather.mvc.model.AverageForecastModel( " +
            "    f.time, " +
            "    avg(f.precipIntensity), " +
            "    avg(f.temperature), " +
            "    avg(f.dewPoint)," +
            "    avg(f.humidity) * 100," +
            "    avg(f.windSpeed)," +
            "    avg(f.pressure) " +
            ") " +
            " FROM Forecast f " +
            " LEFT JOIN f.weather w " +
            " WHERE w.location = :location " +
            " GROUP BY f.time " +
            " ORDER BY f.time desc" )
    List<AverageForecastModel> getCalculatedForecastForLocation(@Param("location") Location location);

}
