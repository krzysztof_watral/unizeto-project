package pl.dreamware.weather.util;

import org.apache.commons.lang3.time.DateUtils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;

public final class DateTimeUtils {

    private DateTimeUtils() {
        //utility class
    }

    public static Date nearestHourOf(Date date) {
        return DateUtils.round(date, Calendar.HOUR);
    }

    public static Date currentHour() {
        return nearestHourOf(Calendar.getInstance().getTime());
    }

    public static Date getPreviousUTCDateTime(int previousDays) {
        return Date.from(LocalDateTime.now(ZoneOffset.UTC).minusDays(previousDays).toInstant(ZoneOffset.UTC));
    }

    public static boolean isInProperRange(Date date, int previousDays) {
        return date.compareTo(getPreviousUTCDateTime(previousDays)) > 0 && date.compareTo(new Date()) < 0;
    }
}
