package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.FeedDataModel;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundObservation implements FeedDataModel {

    private static final long serialVersionUID = -5429046311705982342L;

    @JsonProperty("display_location")
    private WundergroundLocation location;

    @JsonProperty("local_epoch")
    private Integer localEpoch;

    @JsonProperty("temp_f")
    private Double tempF;

    @JsonProperty("relative_humidity")
    private String relativeHumidity;

    @JsonProperty("wind_mph")
    private Double windMph;

    @JsonProperty("pressure_mb")
    private Double pressureMb;

    @JsonProperty("dewpoint_f")
    private Double dewpointF;

    @JsonProperty("precip_1hr_metric")
    private Double precip1hrMetric;

    public WundergroundLocation getLocation() {
        return location;
    }

    public Date getDate() {
        return new Date(localEpoch * 1000l);
    }

    public Integer getLocalEpoch() {
        return localEpoch;
    }

    public void setLocalEpoch(String localEpoch) {
        this.localEpoch = ConversionUtils.asInteger(localEpoch);
    }

    public Double getTempF() {
        return tempF;
    }

    public void setTempF(String tempF) {
        this.tempF = ConversionUtils.asDouble(tempF);
    }

    public String getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(String relativeHumidity) {
        this.relativeHumidity = ConversionUtils.asString(relativeHumidity);
    }

    public Double getWindMph() {
        return windMph;
    }

    public void setWindMph(String windMph) {
        this.windMph = ConversionUtils.asDouble(windMph);
    }

    public Double getPressureMb() {
        return pressureMb;
    }

    public void setPressureMb(String pressureMb) {
        this.pressureMb = ConversionUtils.asDouble(pressureMb);
    }

    public Double getDewpointF() {
        return dewpointF;
    }

    public void setDewpointF(String dewpointF) {
        this.dewpointF = ConversionUtils.asDouble(dewpointF);
    }

    public Double getPrecip1hrMetric() {
        return precip1hrMetric;
    }

    public void setPrecip1hrMetric(String precip1hrMetric) {
        this.precip1hrMetric = ConversionUtils.asDouble(precip1hrMetric);
    }

}
