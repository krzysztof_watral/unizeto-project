package pl.dreamware.weather.api.base.service;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.util.DateTimeUtils;

public abstract class BaseHistoryDataFetcher<T extends RootFeedDataModel> extends BaseDataFetcher<T>
		implements HistoryDataFetcher<Weather> {

	public static final int ACCEPTED_PREVIOUS_DAYS_NUMBER = 3;

	@Autowired
	protected RestTemplate restTemplate;

	public BaseHistoryDataFetcher(Converter<T, Weather> converter, Class<T> clazz) {
		super(converter, clazz);
	}

	@Override
	public Weather getHistoryData(Location location) {
		Set<Date> unique = new HashSet<>();
		List<Forecast> forecasts = getHistoricalApiUrls(location).stream()
				.map(this::downloadData)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.map(converter::convert)
				.filter(Objects::nonNull)
				.flatMap(weather -> weather.getForecasts().stream())
				.filter(f -> unique.add(f.getTime()))
				.filter(f -> DateTimeUtils.isInProperRange(f.getTime(), ACCEPTED_PREVIOUS_DAYS_NUMBER))
				.collect(Collectors.toList());

		Weather weather = new Weather();
		weather.addForecasts(forecasts);
		return weather;
	}

	protected abstract List<String> getHistoricalApiUrls(Location location);

}
