package pl.dreamware.weather.api.wunderground.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.wunderground.model.WundergroundObservation;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.util.DateTimeUtils;

@Component
class WundergroundObservationConverter extends Converter<WundergroundObservation, Forecast> {

    public Forecast convert(WundergroundObservation item) {
        Forecast forecast = new Forecast();

        if (StringUtils.isNotBlank(item.getRelativeHumidity())) {
            Double humidity = Double.valueOf(item.getRelativeHumidity().replace("%", "")) / 100d;
            forecast.setHumidity(humidity);
        }
        forecast.setDewPoint(inCelsius(item.getDewpointF()));
        forecast.setPrecipIntensity(inMm(item.getPrecip1hrMetric()));
        forecast.setPressure(item.getPressureMb());
        forecast.setTemperature(inCelsius(item.getTempF()));
        forecast.setWindSpeed(inKilometers(item.getWindMph()));
        forecast.setTime(DateTimeUtils.nearestHourOf(item.getDate()));

        return forecast;
    }
}