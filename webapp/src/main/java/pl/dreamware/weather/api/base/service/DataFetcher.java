package pl.dreamware.weather.api.base.service;

import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Model;

public interface DataFetcher<R extends Model> extends Source {

	R fetchData(Location location);

}
