package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.FeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundLocation implements FeedDataModel {

    @JsonProperty("full")
    private String place;

    @JsonProperty("latitude")
    private Double latitude;

    @JsonProperty("longitude")
    private Double longitude;

    public String getPlace() {
        return place;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setPlace(String place) {
        this.place = ConversionUtils.asString(place);
    }

    public void setLatitude(String latitude) {
        this.latitude = ConversionUtils.asDouble(latitude);
    }

    public void setLongitude(String longitude) {
        this.longitude = ConversionUtils.asDouble(longitude);
    }
}
