package pl.dreamware.weather.api.wunderground.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.wunderground.model.WundergroundHistoryContainer;
import pl.dreamware.weather.api.wunderground.model.WundergroundHistoryModel;
import pl.dreamware.weather.api.wunderground.model.WundergroundHistoryObservation;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Weather;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class WundergroundHistoryConverter extends Converter<WundergroundHistoryModel, Weather> {

    @Autowired
    private WundergroundHistoryObservationConverter wundergroundHistoryObservationConverter;

    @Override
    public Weather convert(WundergroundHistoryModel feedDataModel) {
        WundergroundHistoryContainer container = feedDataModel.getContainer();
        Weather weather = null;
        if (container != null) {
            Collection<WundergroundHistoryObservation> observations = container.getObservations();
            List<Forecast> forecasts = observations.stream()
                    .map(wundergroundHistoryObservationConverter::convert)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            weather = new Weather();
            weather.addForecasts(forecasts);
        }
        return weather;
    }
}
