package pl.dreamware.weather.api.wunderground.service;

import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.api.base.service.Source;

import java.text.SimpleDateFormat;

interface WundergroundApi extends Source {

    String API = "http://api.wunderground.com/api/%s";

    String CURRENT_API_URL = API + "/conditions/q/%s,%s.json";
    
    String HISTORY_API_URL = API + "/history_%s/q/%s,%s.json";

    String LOCATION_API_URL = API + "/conditions/q/%s.json";

    SimpleDateFormat API_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");

    default SourceService service() {
        return SourceService.WUNDERGROUND;
    }
}
