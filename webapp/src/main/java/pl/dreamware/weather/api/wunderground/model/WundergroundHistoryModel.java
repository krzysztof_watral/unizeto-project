package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundHistoryModel implements RootFeedDataModel {

	private static final long serialVersionUID = 8165204835677779119L;
	
	@JsonProperty("history")
	WundergroundHistoryContainer container;

	public WundergroundHistoryContainer getContainer() {
		return container;
	}

	public void setContainer(WundergroundHistoryContainer container) {
		this.container = container;
	}
}
