package pl.dreamware.weather.api.wunderground.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.wunderground.model.WundergroundModel;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Weather;

import java.util.Collections;

@Component
public class WundergroundConverter extends Converter<WundergroundModel, Weather> {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WundergroundObservationConverter wundergroundObservationConverter;

    @Autowired
    private WundergroundLocationConverter wundergroundLocationConverter;

    @Override
    public Weather convert(WundergroundModel model) {
        if (model.getResponse().getError() == null) {
            Weather weather = new Weather();
            Forecast forecast = wundergroundObservationConverter.convert(model.getCurrentObservation());
            weather.addForecasts(Collections.singletonList(forecast));
            weather.setLocation(wundergroundLocationConverter.convert(model.getCurrentObservation().getLocation()));
            return weather;
        }
        LOG.error("ERROR - MISSING DATA : " + model.getResponse().getError().getDescription());
        return null;
    }
}
