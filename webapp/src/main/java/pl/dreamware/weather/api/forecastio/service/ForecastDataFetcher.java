package pl.dreamware.weather.api.forecastio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dreamware.weather.api.base.service.BaseCurrentDataFetcher;
import pl.dreamware.weather.api.forecastio.converter.ForecastConverter;
import pl.dreamware.weather.api.forecastio.model.ForecastModel;
import pl.dreamware.weather.entity.Location;

import java.util.Date;

@Service
class ForecastDataFetcher extends BaseCurrentDataFetcher<ForecastModel> implements ForecastApi {

    @Autowired
    public ForecastDataFetcher(ForecastConverter converter) {
        super(converter, ForecastModel.class);
    }

    @Override
    protected String getApiUrl(Location location) {
        return getApiUrl(location, new Date());
    }

    private String getApiUrl(Location location, Date date) {
        return String.format(API_URL,
                apiProperties.getForecastioApiKey(),
                location.getLatitude(),
                location.getLatitude(),
                API_DATE_FORMAT.format(date));
    }
}
