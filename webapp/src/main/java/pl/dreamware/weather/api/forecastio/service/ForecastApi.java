package pl.dreamware.weather.api.forecastio.service;

import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.api.base.service.Source;

import java.text.SimpleDateFormat;

interface ForecastApi extends Source {

    String API_URL = "https://api.forecast.io/forecast/%s/%s,%s,%s";

    SimpleDateFormat API_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    default SourceService service() {
        return SourceService.FORECAST_IO;
    }
}
