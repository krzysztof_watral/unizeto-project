package pl.dreamware.weather.api.forecastio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dreamware.weather.api.base.service.BaseHistoryDataFetcher;
import pl.dreamware.weather.api.forecastio.converter.ForecastHistoryConverter;
import pl.dreamware.weather.api.forecastio.model.ForecastHistoryModel;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.util.DateTimeUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
class ForecastHistoryDataFetcher extends BaseHistoryDataFetcher<ForecastHistoryModel> implements ForecastApi {

    protected static final int PREVIOUS_DAYS_FOR_HISTORICAL_DATA = 3;

    @Autowired
    public ForecastHistoryDataFetcher(ForecastHistoryConverter converter) {
        super(converter, ForecastHistoryModel.class);
    }

    @Override
    protected List<String> getHistoricalApiUrls(Location location) {
        return IntStream.rangeClosed(0, PREVIOUS_DAYS_FOR_HISTORICAL_DATA)
                .mapToObj(DateTimeUtils::getPreviousUTCDateTime)
                .map(date -> getApiUrl(location, date)).collect(Collectors.toList());
    }

    private String getApiUrl(Location location, Date date) {
        return String.format(API_URL,
                apiProperties.getForecastioApiKey(),
                location.getLatitude(),
                location.getLatitude(),
                API_DATE_FORMAT.format(date));
    }
}
