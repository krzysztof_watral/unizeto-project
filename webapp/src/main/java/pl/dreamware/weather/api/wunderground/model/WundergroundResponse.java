package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundResponse {

    @JsonProperty("error")
    private WundergroundError error;

    @JsonProperty("results")
    private WundergroundResult[] results;

    public WundergroundError getError() {
        return error;
    }

    public WundergroundResult[] getResults() {
        return results;
    }
}
