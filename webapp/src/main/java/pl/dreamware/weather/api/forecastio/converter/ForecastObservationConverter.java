package pl.dreamware.weather.api.forecastio.converter;

import org.springframework.stereotype.Component;

import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.forecastio.model.ForecastObservation;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.util.DateTimeUtils;

@Component
class ForecastObservationConverter extends Converter<ForecastObservation, Forecast> {

	public Forecast convert(ForecastObservation item) {
		Forecast forecast = new Forecast();

		forecast.setDewPoint(inCelsius(item.getDewPoint()));
		forecast.setHumidity(item.getHumidity());
		forecast.setPrecipIntensity(inMm(item.getPrecipIntensity()));
		forecast.setPressure(item.getPressure());
		forecast.setTemperature(inCelsius(item.getTemperature()));
		forecast.setWindSpeed(inKilometers(item.getWindSpeed()));
		forecast.setTime(DateTimeUtils.nearestHourOf(item.getDate()));

		return forecast;
	}
}
