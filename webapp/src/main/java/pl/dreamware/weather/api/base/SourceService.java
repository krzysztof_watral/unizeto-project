package pl.dreamware.weather.api.base;

public enum SourceService {
    FORECAST_IO, WUNDERGROUND;
}
