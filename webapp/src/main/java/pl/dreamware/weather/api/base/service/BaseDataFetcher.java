package pl.dreamware.weather.api.base.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import pl.dreamware.weather.api.base.ApiProperties;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;
import pl.dreamware.weather.entity.Weather;

public abstract class BaseDataFetcher<T extends RootFeedDataModel> {

	private final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	protected ApiProperties apiProperties;

	private final Class<T> clazz;

	protected final Converter<T, Weather> converter;

	public BaseDataFetcher(Converter<T, Weather> converter, Class<T> clazz) {
		this.converter = converter;
		this.clazz = clazz;
	}

	protected Optional<T> downloadData(String url) {
		T result = null;
		try {
			LOG.debug("rest service: " + url);
			result = restTemplate.getForObject(url, clazz);
		} catch (RestClientException ex) {
			LOG.error("Error during connection to external rest services: url", ex);
		}
		return Optional.ofNullable(result);
	}

}
