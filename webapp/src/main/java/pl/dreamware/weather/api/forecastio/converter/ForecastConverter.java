package pl.dreamware.weather.api.forecastio.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.forecastio.model.ForecastModel;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Weather;

@Component
public class ForecastConverter extends Converter<ForecastModel, Weather> {

	@Autowired
	private ForecastObservationConverter forecastObservationConverter;

	@Override
	public Weather convert(ForecastModel feedDataModel) {
		Forecast forecast = forecastObservationConverter.convert(feedDataModel.getCurrently());
		Weather weather = new Weather();
		weather.addForecast(forecast);
		return weather;
	}
}
