package pl.dreamware.weather.api;

import org.apache.commons.lang3.StringUtils;

public final class ConversionUtils {

	private ConversionUtils() {
		// utility class
	}

	public static Double asDouble(String value) {
		Double result = null;
		try {
			result = isNotValid(value) ? null : Math.max(0, Double.valueOf(value.trim()));
		} catch (Exception e) {
			//ignore
		}
		return result;
	}

	public static Integer asInteger(String value) {
		Integer result = null;
		try {
			return isNotValid(value) ? null : Math.max(0, Integer.valueOf(value.trim()));
		} catch (Exception e) {
			//ignore
		}
		return null;
	}

	public static String asString(String value) {
		return isNotValid(value) ? null : value.trim();
	}

	private static boolean isNotValid(String value) {
		return StringUtils.isBlank(value)
				|| "--".equalsIgnoreCase(value)
				|| "-".equalsIgnoreCase(value)
				|| "NA".equalsIgnoreCase(value)
				|| "N/A".equalsIgnoreCase(value);
	}

}
