package pl.dreamware.weather.api.wunderground.converter;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.wunderground.model.WundergroundDate;
import pl.dreamware.weather.api.wunderground.model.WundergroundHistoryObservation;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.util.DateTimeUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Component
class WundergroundHistoryObservationConverter extends Converter<WundergroundHistoryObservation, Forecast> {

    public Forecast convert(WundergroundHistoryObservation item) {
        Forecast forecast = new Forecast();

        if (StringUtils.isNotBlank(item.getRelativeHumidity())) {
            Double humidity = Double.valueOf(item.getRelativeHumidity()) / 100d;
            forecast.setHumidity(humidity);
        }
        forecast.setDewPoint(inCelsius(item.getDewpointF()));
        forecast.setPrecipIntensity(inMm(item.getPrecip1hrMetric()));
        forecast.setPressure(item.getPressureMb());
        forecast.setTemperature(inCelsius(item.getTempF()));
        forecast.setWindSpeed(inKilometers(item.getWindMph()));
        forecast.setTime(DateTimeUtils.nearestHourOf(utcDateTimeOf(item)));

        return forecast;
    }

    private Date utcDateTimeOf(WundergroundHistoryObservation wundergroundHistoryObservation) {
        WundergroundDate d = wundergroundHistoryObservation.getUtcDate();
        LocalDateTime dateTime = LocalDateTime.of(d.getYear(), d.getMonth(), d.getMonthDay(), d.getHour(), d.getMin());
        return Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}