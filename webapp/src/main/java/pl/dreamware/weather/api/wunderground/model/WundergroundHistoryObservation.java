package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.FeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundHistoryObservation implements FeedDataModel {

    private static final long serialVersionUID = 836207945164732023L;

    @JsonProperty("date")
    WundergroundDate localDate;

    @JsonProperty("utcdate")
    WundergroundDate utcDate;

    @JsonProperty("tempi")
    private Double tempF;

    @JsonProperty("hum")
    private String relativeHumidity;

    @JsonProperty("wspdi")
    private Double windMph;

    @JsonProperty("pressurem")
    private Double pressureMb;

    @JsonProperty("dewpti")
    private Double dewpointF;

    @JsonProperty(value = "precipi")
    private Double precip1hrMetric;

    public Double getTempF() {
        return tempF;
    }

    public void setTempF(String tempF) {
        this.tempF = ConversionUtils.asDouble(tempF);
    }

    public String getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(String relativeHumidity) {
        this.relativeHumidity = ConversionUtils.asString(relativeHumidity);
    }

    public Double getWindMph() {
        return windMph;
    }

    public void setWindMph(String windMph) {
        this.windMph = ConversionUtils.asDouble(windMph);
    }

    public Double getPressureMb() {
        return pressureMb;
    }

    public void setPressureMb(String pressureMb) {
        this.pressureMb = ConversionUtils.asDouble(pressureMb);
    }

    public Double getDewpointF() {
        return dewpointF;
    }

    public void setDewpointF(String dewpointF) {
        this.dewpointF = ConversionUtils.asDouble(dewpointF);
    }

    public Double getPrecip1hrMetric() {
        return precip1hrMetric;
    }

    public void setPrecip1hrMetric(String precip1hrMetric) {
        this.precip1hrMetric = ConversionUtils.asDouble(precip1hrMetric);
    }

    public WundergroundDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(WundergroundDate localDate) {
        this.localDate = localDate;
    }

    public WundergroundDate getUtcDate() {
        return utcDate;
    }

    public void setUtcDate(WundergroundDate utcDate) {
        this.utcDate = utcDate;
    }

}
