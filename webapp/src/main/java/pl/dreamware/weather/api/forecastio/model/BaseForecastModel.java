package pl.dreamware.weather.api.forecastio.model;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;

public abstract class BaseForecastModel implements RootFeedDataModel {

	private Double latitude;

	private Double longitude;

	private String timezone;

	private HourlyInfo hourly;

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public String getTimezone() {
		return this.timezone;
	}

	public HourlyInfo getHourly() {
		return this.hourly;
	}

	public void setLatitude(String latitude) {
		this.latitude = ConversionUtils.asDouble(latitude);
	}

	public void setLongitude(String longitude) {
		this.longitude = ConversionUtils.asDouble(longitude);
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public void setHourly(HourlyInfo hourly) {
		this.hourly = hourly;
	}
}