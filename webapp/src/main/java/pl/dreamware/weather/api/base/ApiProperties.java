package pl.dreamware.weather.api.base;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiProperties {

    @Value("${pl.dreamware.weather.api.key.forecastio}")
    private String forecastioApiKey;

    @Value("${pl.dreamware.weather.api.key.wunderground}")
    private String wundergroundApiKey;

    public String getForecastioApiKey() {
        return forecastioApiKey;
    }

    public String getWundergroundApiKey() {
        return wundergroundApiKey;
    }
}
