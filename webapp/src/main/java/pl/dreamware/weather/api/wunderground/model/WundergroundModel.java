package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundModel implements RootFeedDataModel {

    @JsonProperty("response")
    private WundergroundResponse response;

    @JsonProperty("current_observation")
    private WundergroundObservation currentObservation;

    public WundergroundResponse getResponse() {
        return response;
    }

    public WundergroundObservation getCurrentObservation() {
        return currentObservation;
    }

    public void setCurrentObservation(WundergroundObservation currentObservation) {
        this.currentObservation = currentObservation;
    }
}
