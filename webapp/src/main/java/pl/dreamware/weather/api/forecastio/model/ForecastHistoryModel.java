package pl.dreamware.weather.api.forecastio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import pl.dreamware.weather.api.base.model.RootFeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastHistoryModel extends BaseForecastModel implements RootFeedDataModel {

	private HourlyInfo hourly;

	public HourlyInfo getHourly() {
		return this.hourly;
	}

	public void setHourly(HourlyInfo hourly) {
		this.hourly = hourly;
	}
}
