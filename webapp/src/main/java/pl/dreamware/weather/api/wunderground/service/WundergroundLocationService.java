package pl.dreamware.weather.api.wunderground.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dreamware.weather.api.base.service.BaseDataFetcher;
import pl.dreamware.weather.api.base.service.LocationDataFetcher;
import pl.dreamware.weather.api.wunderground.converter.WundergroundConverter;
import pl.dreamware.weather.api.wunderground.model.WundergroundModel;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;

import java.util.Optional;

@Service
class WundergroundLocationService extends BaseDataFetcher<WundergroundModel> implements WundergroundApi, LocationDataFetcher {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public WundergroundLocationService(WundergroundConverter converter) {
        super(converter, WundergroundModel.class);
    }

    @Override
    public Location tryToGetLocation(String place) {
        String url = getApiUrl(place);
        Optional<WundergroundModel> optionalModel = downloadData(url);
        if (optionalModel.isPresent()) {
            WundergroundModel model = optionalModel.get();
            if (isLocationExplicit(model)) {
                Weather weather = converter.convert(model);
                return weather != null ? weather.getLocation() : null;
            } else if (model.getResponse().getError() != null) {
                LOG.error("API RESPONSE: " + model.getResponse().getError().getDescription());
            }
        }
        return null;
    }

    private boolean isLocationExplicit(WundergroundModel model) {
        return model.getResponse().getResults() == null && model.getCurrentObservation() != null;
    }

    private String getApiUrl(String place) {
        return String.format(LOCATION_API_URL, apiProperties.getWundergroundApiKey(), place);
    }
}
