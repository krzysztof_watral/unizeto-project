package pl.dreamware.weather.api.wunderground.converter;

import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.wunderground.model.WundergroundLocation;
import pl.dreamware.weather.entity.Location;

@Component
public class WundergroundLocationConverter extends Converter<WundergroundLocation, Location> {

    @Override
    public Location convert(WundergroundLocation model) {
        Location location = new Location();
        location.setPlace(model.getPlace());
        location.setLatitude(model.getLatitude());
        location.setLongitude(model.getLongitude());
        return location;
    }
}