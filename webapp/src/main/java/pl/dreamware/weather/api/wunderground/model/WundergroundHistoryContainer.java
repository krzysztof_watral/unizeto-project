package pl.dreamware.weather.api.wunderground.model;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.dreamware.weather.api.base.model.FeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundHistoryContainer implements FeedDataModel {

	private static final long serialVersionUID = 98016593105168810L;

	@JsonProperty("date")
	WundergroundDate localDate;

	@JsonProperty("utcdate")
	WundergroundDate utcDate;

	@JsonProperty("observations")
	Collection<WundergroundHistoryObservation> observations;

	public WundergroundDate getLocalDate() {
		return localDate;
	}

	public void setLocalDate(WundergroundDate localDate) {
		this.localDate = localDate;
	}

	public WundergroundDate getUtcDate() {
		return utcDate;
	}

	public void setUtcDate(WundergroundDate utcDate) {
		this.utcDate = utcDate;
	}

	public Collection<WundergroundHistoryObservation> getObservations() {
		return observations;
	}

	public void setObservations(Collection<WundergroundHistoryObservation> observations) {
		this.observations = observations;
	}
	
}
