package pl.dreamware.weather.api.forecastio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastModel extends BaseForecastModel implements RootFeedDataModel {

    private ForecastObservation currently;

    public ForecastObservation getCurrently() {
        return currently;
    }

    public void setCurrently(ForecastObservation currently) {
        this.currently = currently;
    }
}