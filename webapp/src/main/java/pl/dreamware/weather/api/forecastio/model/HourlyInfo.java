package pl.dreamware.weather.api.forecastio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class HourlyInfo {

    private ForecastObservation[] data;

    public ForecastObservation[] getData() {
        return this.data;
    }

    public void setData(ForecastObservation[] data) {
        this.data = data;
    }
}