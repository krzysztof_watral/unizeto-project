package pl.dreamware.weather.api.forecastio.converter;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.forecastio.model.ForecastHistoryModel;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Weather;

@Component
public class ForecastHistoryConverter extends Converter<ForecastHistoryModel, Weather> {

	@Autowired
	private ForecastObservationConverter forecastObservationConverter;

	@Override
	public Weather convert(ForecastHistoryModel feedDataModel) {
		List<Forecast> forecasts = Arrays.stream(feedDataModel.getHourly().getData())
				.map(forecastObservationConverter::convert)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		Weather weather = new Weather();
		weather.addForecasts(forecasts);

		return weather;
	}
}
