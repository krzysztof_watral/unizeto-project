package pl.dreamware.weather.api.wunderground.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dreamware.weather.api.base.service.BaseHistoryDataFetcher;
import pl.dreamware.weather.api.wunderground.converter.WundergroundHistoryConverter;
import pl.dreamware.weather.api.wunderground.model.WundergroundHistoryModel;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.util.DateTimeUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
class WundergroundHistoryDataFetcher extends BaseHistoryDataFetcher<WundergroundHistoryModel> implements WundergroundApi {

    private static final int PREVIOUS_DAYS_FOR_HISTORICAL_DATA = 4;

    @Autowired
    public WundergroundHistoryDataFetcher(WundergroundHistoryConverter converter) {
        super(converter, WundergroundHistoryModel.class);
    }

    @Override
    protected List<String> getHistoricalApiUrls(Location location) {
        return IntStream.rangeClosed(0, PREVIOUS_DAYS_FOR_HISTORICAL_DATA)
                .mapToObj(DateTimeUtils::getPreviousUTCDateTime)
                .map(date -> getApiUrl(location, date)).collect(Collectors.toList());
    }

    private String getApiUrl(Location location, Date date) {
        return String.format(HISTORY_API_URL,
                apiProperties.getWundergroundApiKey(),
                API_DATE_FORMAT.format(date),
                location.getLatitude(),
                location.getLongitude());
    }
}
