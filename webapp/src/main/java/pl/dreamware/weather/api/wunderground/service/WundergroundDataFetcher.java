package pl.dreamware.weather.api.wunderground.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.dreamware.weather.api.base.service.BaseCurrentDataFetcher;
import pl.dreamware.weather.api.wunderground.converter.WundergroundConverter;
import pl.dreamware.weather.api.wunderground.model.WundergroundModel;
import pl.dreamware.weather.entity.Location;

@Service
class WundergroundDataFetcher extends BaseCurrentDataFetcher<WundergroundModel> implements WundergroundApi {

    @Autowired
    public WundergroundDataFetcher(WundergroundConverter converter) {
        super(converter, WundergroundModel.class);
    }

    @Override
    protected String getApiUrl(Location location) {
        return String.format(CURRENT_API_URL,
                apiProperties.getWundergroundApiKey(),
                location.getLatitude(),
                location.getLongitude());
    }
}