package pl.dreamware.weather.api.forecastio.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.FeedDataModel;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ForecastObservation implements FeedDataModel {

    private int time;

    private Double precipIntensity;

    private Double temperature;

    private Double apparentTemperature;

    private Double dewPoint;

    private Double humidity;

    private Double windSpeed;

    private Double pressure;

    public Date getDate() {
        return new Date(time * 1000l);
    }

    public int getTime() {
        return this.time;
    }

    public Double getPrecipIntensity() {
        return this.precipIntensity;
    }

    public Double getTemperature() {
        return this.temperature;
    }

    public Double getApparentTemperature() {
        return this.apparentTemperature;
    }

    public Double getDewPoint() {
        return this.dewPoint;
    }

    public Double getHumidity() {
        return this.humidity;
    }

    public Double getWindSpeed() {
        return this.windSpeed;
    }

    public Double getPressure() {
        return this.pressure;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setPrecipIntensity(String precipIntensity) {
        this.precipIntensity = ConversionUtils.asDouble(precipIntensity);
    }

    public void setTemperature(String temperature) {
        this.temperature = ConversionUtils.asDouble(temperature);
    }

    public void setApparentTemperature(String apparentTemperature) {
        this.apparentTemperature = ConversionUtils.asDouble(apparentTemperature);
    }

    public void setDewPoint(String dewPoint) {
        this.dewPoint = ConversionUtils.asDouble(dewPoint);
    }

    public void setHumidity(String humidity) {
        this.humidity = ConversionUtils.asDouble(humidity);
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = ConversionUtils.asDouble(windSpeed);
    }

    public void setPressure(String pressure) {
        this.pressure = ConversionUtils.asDouble(pressure);
    }
}