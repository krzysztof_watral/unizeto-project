package pl.dreamware.weather.api.wunderground.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import pl.dreamware.weather.api.ConversionUtils;
import pl.dreamware.weather.api.base.model.FeedDataModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WundergroundDate implements FeedDataModel {

	private static final long serialVersionUID = 8882402519536668536L;

	@JsonProperty("pretty")
	private String pretty;

	@JsonProperty("year")
	private Integer year;

	@JsonProperty("mon")
	private Integer month;

	@JsonProperty("mday")
	private Integer monthDay;

	@JsonProperty("hour")
	private Integer hour;

	@JsonProperty("min")
	private Integer min;

	@JsonProperty("tzname")
	private String timeZoneName;

	public String getPretty() {
		return pretty;
	}

	public void setPretty(String pretty) {
		this.pretty = pretty;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = ConversionUtils.asInteger(year);
	}

	public Integer getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = ConversionUtils.asInteger(month);
	}

	public Integer getMonthDay() {
		return monthDay;
	}

	public void setMonthDay(String monthDay) {
		this.monthDay = ConversionUtils.asInteger(monthDay);
	}

	public Integer getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = ConversionUtils.asInteger(hour);
	}

	public Integer getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = ConversionUtils.asInteger(min);
	}

	public String getTimeZoneName() {
		return timeZoneName;
	}

	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

}
