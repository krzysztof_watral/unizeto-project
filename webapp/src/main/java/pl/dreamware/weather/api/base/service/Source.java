package pl.dreamware.weather.api.base.service;

import pl.dreamware.weather.api.base.SourceService;

public interface Source {

	SourceService service();
}
