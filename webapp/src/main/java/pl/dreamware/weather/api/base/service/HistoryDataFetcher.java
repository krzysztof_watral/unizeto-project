package pl.dreamware.weather.api.base.service;

import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Model;

public interface HistoryDataFetcher<R extends Model> extends Source {

	R getHistoryData(Location location);
}
