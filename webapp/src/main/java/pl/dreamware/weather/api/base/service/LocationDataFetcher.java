package pl.dreamware.weather.api.base.service;

import pl.dreamware.weather.entity.Location;

public interface LocationDataFetcher {

	Location tryToGetLocation(String place);
}
