package pl.dreamware.weather.api.base.service;

import java.util.Optional;

import pl.dreamware.weather.api.base.converter.Converter;
import pl.dreamware.weather.api.base.model.RootFeedDataModel;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;

public abstract class BaseCurrentDataFetcher<T extends RootFeedDataModel> extends BaseDataFetcher<T>
		implements DataFetcher<Weather> {

	public BaseCurrentDataFetcher(Converter<T, Weather> converter, Class<T> clazz) {
		super(converter, clazz);
	}

	@Override
	public Weather fetchData(Location location) {
		String url = getApiUrl(location);
		Optional<T> forObject = downloadData(url);
		return forObject.map(converter::convert).orElse(null);
	}

	protected abstract String getApiUrl(Location location);

}
