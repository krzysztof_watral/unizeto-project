package pl.dreamware.weather.api.base.converter;

import pl.dreamware.weather.api.base.model.FeedDataModel;
import pl.dreamware.weather.entity.Model;

/**
 * Base converter class to convert object type between {@link FeedDataModel} and {@link Model}, contains default methods (feature of java 8)
 *
 * @param <F> extends {@link FeedDataModel}
 * @param <M> extends {@link Model}
 */
public abstract class Converter<F extends FeedDataModel, M extends Model> {

	public abstract M convert(F feedDataModel);

	protected Double inCelsius(Double fahrenheit) {
		return fahrenheit == null ? null : (fahrenheit - 32) * 5 / 9d;
	}

	protected Double inKilometers(Double mile) {
		return mile == null ? null : mile * 1.609344;
	}

	protected Double inMm(Double in) {
		return in == null ? null : in * 25.4;
	}

}
