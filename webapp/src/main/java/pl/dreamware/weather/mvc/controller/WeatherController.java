package pl.dreamware.weather.mvc.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.dreamware.weather.mvc.model.AverageForecastModel;
import pl.dreamware.weather.mvc.model.LocationFormModel;
import pl.dreamware.weather.service.WeatherService;

import java.util.List;

@Controller
@SessionAttributes("locationFormModel")
public class WeatherController {

    @Autowired
    private WeatherService weatherService;

    private static final String MAIN_PAGE = "/";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView mainPage() {
        return new ModelAndView("pages/weather")
                .addObject("defineLocationModel", new LocationFormModel())
                .addObject("locations", weatherService.getAllLocations());
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public ModelAndView submit(LocationFormModel locationFormModel, RedirectAttributes attr) {
        if (StringUtils.isNotBlank(locationFormModel.getPlace())) {
            attr.addAttribute("valid", weatherService.defineLocation(locationFormModel.getPlace()));
        }
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/remove/{id}")
    public ModelAndView remove(@PathVariable final long id) {
        weatherService.removeLocation(id);
        return new ModelAndView("redirect:/");
    }

    @RequestMapping(value = "/{id}/{place}")
    public ModelAndView weatherDetails(@PathVariable final long id, @PathVariable final String place) {
        List<AverageForecastModel> calculated = weatherService.getCalculatedForecastForLocation(id);
        return new ModelAndView("pages/details").addObject("calculated", calculated).addObject("place", place);
    }

}
