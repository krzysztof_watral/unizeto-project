package pl.dreamware.weather.mvc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.mvc.model.AverageForecastModel;
import pl.dreamware.weather.service.WeatherService;

import java.util.List;

@RestController
public class WeatherRestController {

    @Autowired
    private WeatherService weatherService;

    @RequestMapping(value = "/api/locations", method = RequestMethod.GET)
    public List<Location> getLocations() {
        return weatherService.getAllLocations();
    }

    @RequestMapping(value = "/api/locations", method = RequestMethod.POST)
    public Boolean addLocation(@RequestBody String place) {
        return weatherService.defineLocation(place);
    }

    @RequestMapping(value = "/api/locations/{id}", method = RequestMethod.GET)
    public List<AverageForecastModel> getForecast(@PathVariable Long id) {
        return weatherService.getCalculatedForecastForLocation(id.longValue());
    }

    @RequestMapping(value = "/api/locations/{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable Long id) {
        weatherService.removeLocation(id);
    }
}