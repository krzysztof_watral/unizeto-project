package pl.dreamware.weather.mvc.model;

public class LocationFormModel {

    private String place;

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }
}
