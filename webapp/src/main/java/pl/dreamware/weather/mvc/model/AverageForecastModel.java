package pl.dreamware.weather.mvc.model;

import java.util.Date;

public class AverageForecastModel {

    private final Date time;

    private final Double precipIntensity;

    private final Double temperature;

    private final Double dewPoint;

    private final Double humidity;

    private final Double windSpeed;

    private final Double pressure;

    public AverageForecastModel(Date time, Double precipIntensity, Double temperature, Double dewPoint, Double humidity, Double windSpeed, Double pressure) {
        this.time = time;
        this.precipIntensity = precipIntensity == null ? 0 : precipIntensity;
        this.temperature = temperature == null ? 0 : temperature;
        this.dewPoint = dewPoint == null ? 0 : dewPoint;
        this.humidity = humidity == null ? 0 : humidity;
        this.windSpeed = windSpeed == null ? 0 : windSpeed;
        this.pressure = pressure == null ? 0 : pressure;
    }

    public Date getTime() {
        return time;
    }

    public Double getPrecipIntensity() {
        return precipIntensity;
    }

    public Double getTemperature() {
        return temperature;
    }

    public Double getDewPoint() {
        return dewPoint;
    }

    public Double getHumidity() {
        return humidity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public Double getPressure() {
        return pressure;
    }
}
