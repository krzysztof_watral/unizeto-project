package pl.dreamware.weather.service;

import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.mvc.model.AverageForecastModel;

import java.util.Date;
import java.util.List;

public interface WeatherService {

    void saveWeather(List<Weather> weatherList);

    void clearWeatherData();

    List<Date> getExistingForecastDates(SourceService sourceService, Location location);

    Long removeOutOfDateData();

    List<Location> getAllLocations();

    boolean defineLocation(String place);

    void removeLocation(long id);

    List<AverageForecastModel> getCalculatedForecastForLocation(long id);
}
