package pl.dreamware.weather.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.dreamware.weather.api.base.SourceService;
import pl.dreamware.weather.api.base.service.BaseHistoryDataFetcher;
import pl.dreamware.weather.api.base.service.LocationDataFetcher;
import pl.dreamware.weather.cron.HistoryWeatherAsync;
import pl.dreamware.weather.dao.ForecastRepository;
import pl.dreamware.weather.dao.LocationRepository;
import pl.dreamware.weather.dao.WeatherRepository;
import pl.dreamware.weather.entity.Forecast;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.mvc.model.AverageForecastModel;
import pl.dreamware.weather.util.DateTimeUtils;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
class WeatherServiceImpl implements WeatherService {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private WeatherRepository weatherRepository;

    @Autowired
    private ForecastRepository forecastRepository;

    @Autowired
    private LocationDataFetcher locationDataFetcher;

    @Autowired
    private HistoryWeatherAsync historyWeatherAsync;

    public List<Location> getAllLocations() {
        return locationRepository.findAll();
    }

    public boolean defineLocation(String place) {
        Location location = locationDataFetcher.tryToGetLocation(place);
        if (location != null) {
            try {
                locationRepository.save(location);
                locationRepository.flush();
                historyWeatherAsync.gatherHistoryDataForLocation(location);
                return true;
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }
        return false;
    }

    @Override
    @Transactional
    public void removeLocation(long id) {
        weatherRepository.removeByLocationId(id);
        locationRepository.delete(id);
    }

    public List<Date> getExistingForecastDates(SourceService sourceService, Location location) {
        return forecastRepository.find(sourceService, location).stream()
                .map(Forecast::getTime)
                .collect(Collectors.toList());
    }

    @Transactional
    public void saveWeather(List<Weather> weatherList) {
        weatherList.stream().forEach(toCreate -> {
            List<Date> datesOfExisting = getExistingForecastDates(toCreate.getSourceService(), toCreate.getLocation());
            removeExistingForecasts(toCreate, datesOfExisting);

            Location location = toCreate.getLocation();
            SourceService service = toCreate.getSourceService();
            Weather toUpdate = weatherRepository.findByLocationAndSourceService(location, service);
            if (toUpdate != null) {
                toUpdate.addForecasts(toCreate.getForecasts());
                weatherRepository.save(toUpdate);
            } else {
                weatherRepository.save(toCreate);
            }
        });
        weatherRepository.flush();
    }

    @Transactional
    public void clearWeatherData() {
        weatherRepository.deleteAll();
        weatherRepository.flush();
    }

    @Transactional
    public Long removeOutOfDateData() {
        Date previousUTCDateTime = DateTimeUtils.getPreviousUTCDateTime(BaseHistoryDataFetcher.ACCEPTED_PREVIOUS_DAYS_NUMBER);
        return forecastRepository.removeByTimeBefore(previousUTCDateTime);
    }

    public List<AverageForecastModel> getCalculatedForecastForLocation(long id) {
        Location location = locationRepository.getOne(id);
        return forecastRepository.getCalculatedForecastForLocation(location);
    }

    private void removeExistingForecasts(Weather weather, List<Date> datesOfExisting) {
        weather.getForecasts().removeIf(f -> datesOfExisting.contains(f.getTime()));
    }
}