package pl.dreamware.weather.entity;

import pl.dreamware.weather.api.base.SourceService;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Weather implements Model {

    private static final long serialVersionUID = -3198586531470147540L;

    @Id
    @GeneratedValue
    private Long id;

    @OrderBy("time ASC")
    @OneToMany(mappedBy = "weather", cascade = {CascadeType.ALL}, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Forecast> forecasts;

    @ManyToOne
    @JoinColumn(name = "location_id")
    private Location location;

    @Enumerated(EnumType.STRING)
    private SourceService sourceService;

    public Long getId() {
        return id;
    }

    public List<Forecast> getForecasts() {
        if (this.forecasts == null) {
            this.forecasts = new ArrayList<>();
        }
        return forecasts;
    }

    public void addForecast(Forecast forecast) {
        forecast.setWeather(this);
        getForecasts().add(forecast);
    }

    public void addForecasts(List<Forecast> forecasts) {
        forecasts.stream().forEach(f -> f.setWeather(this));
        getForecasts().addAll(forecasts);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public SourceService getSourceService() {
        return sourceService;
    }

    public void setSourceService(SourceService sourceService) {
        this.sourceService = sourceService;
    }

}