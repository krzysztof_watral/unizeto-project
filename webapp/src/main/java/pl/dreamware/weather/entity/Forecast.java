package pl.dreamware.weather.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Forecast implements Model {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Date time;

    @Column
    private Double precipIntensity;

    @Column
    private Double temperature;

    @Column
    private Double dewPoint;

    @Column
    private Double humidity;

    @Column
    private Double windSpeed;

    @Column
    private Double pressure;

    @ManyToOne(optional = true)
    @JoinColumn(name = "weather_id")
    private Weather weather;

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Double getTemperature() {
        return temperature;
    }

    public void setTemperature(Double temperature) {
        this.temperature = temperature;
    }

    public Double getPrecipIntensity() {
        return precipIntensity;
    }

    public void setPrecipIntensity(Double precipIntensity) {
        this.precipIntensity = precipIntensity;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Double getPressure() {
        return pressure;
    }

    public void setPressure(Double pressure) {
        this.pressure = pressure;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getDewPoint() {
        return dewPoint;
    }

    public void setDewPoint(Double dewPoint) {
        this.dewPoint = dewPoint;
    }

    public Weather getWeather() {
        return weather;
    }

    void setWeather(Weather weather) {
        this.weather = weather;
    }
}
