package pl.dreamware.weather.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"place"})})
public class Location implements Model {

    private static final long serialVersionUID = -3198586531470147540L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "place", unique = true)
    private String place;

    @Column
    private Double latitude;

    @Column
    private Double longitude;

    @OneToMany(mappedBy = "location")
    private List<Weather> weatherList;

    public Long getId() {
        return id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}