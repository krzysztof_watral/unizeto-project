package pl.dreamware.weather.cron;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.service.DataFetcher;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.service.WeatherService;
import pl.dreamware.weather.util.DateTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
class CurrentWeatherJob implements Job {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private List<DataFetcher<Weather>> dataFetchers;

    @Scheduled(cron = "0 0 * * * *") // every hour
    public void gatherData() {
        List<Weather> weatherList = new ArrayList<>();
        Date currentHourTime = DateTimeUtils.currentHour();
        dataFetchers.forEach(fetcher -> {
            weatherService.getAllLocations().forEach(location -> {
                try {
                    Weather weather = fetcher.fetchData(location);
                    weather.setLocation(location);
                    weather.setSourceService(fetcher.service());
                    weatherList.add(weather);
                    setFullHourTime(weather, currentHourTime);
                } catch (RuntimeException e) {
                    // fe. catch null pointer exception when API key expires!
                    LOG.error(e.getMessage(), e);
                }
            });
        });
        weatherService.saveWeather(weatherList);
    }

    private void setFullHourTime(Weather weather, Date fullHourTime) {
        weather.getForecasts().stream().forEach(f -> f.setTime(fullHourTime));
    }
}
