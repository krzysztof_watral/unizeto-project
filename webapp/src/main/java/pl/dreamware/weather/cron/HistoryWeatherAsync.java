package pl.dreamware.weather.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.service.HistoryDataFetcher;
import pl.dreamware.weather.entity.Location;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.service.WeatherService;

import java.util.ArrayList;
import java.util.List;

@Component
public class HistoryWeatherAsync implements Job {

    @Autowired
    private WeatherService weatherDao;

    @Autowired
    private List<HistoryDataFetcher<Weather>> dataFetchers;

    @Async
    public void gatherHistoryDataForLocation(Location location) {
        List<Weather> weatherList = new ArrayList<>();

        dataFetchers.forEach(fetcher -> {
            Weather weather = fetcher.getHistoryData(location);
            weather.setLocation(location);
            weather.setSourceService(fetcher.service());
            weatherList.add(weather);
        });

        weatherDao.saveWeather(weatherList);
    }
}
