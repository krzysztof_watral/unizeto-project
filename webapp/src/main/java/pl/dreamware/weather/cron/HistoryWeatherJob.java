package pl.dreamware.weather.cron;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.dreamware.weather.api.base.service.HistoryDataFetcher;
import pl.dreamware.weather.entity.Weather;
import pl.dreamware.weather.service.WeatherService;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class HistoryWeatherJob implements Job {

    @Autowired
    private WeatherService weatherService;

    @Autowired
    private List<HistoryDataFetcher<Weather>> dataFetchers;

    @PostConstruct
    public void initData() {
        weatherService.clearWeatherData();
    }

    @Scheduled(fixedRate = 6 * HOUR)
    private void completeMissingData() {
        List<Weather> weatherList = new ArrayList<>();
        dataFetchers.forEach(fetcher ->
                weatherService.getAllLocations().forEach(location -> {
                    Weather weather = fetcher.getHistoryData(location);
                    weather.setLocation(location);
                    weather.setSourceService(fetcher.service());
                    weatherList.add(weather);
                }));
        weatherService.saveWeather(weatherList);
    }

    /**
     * every 15 minutes
     */
    @Scheduled(cron = "0 15 * * * *")
    void removeOld() {
        weatherService.removeOutOfDateData();
    }
}
