package pl.dreamware.weather.cron;

public interface Job {
	int SECOND = 1000;
	int MINUTE = 60 * SECOND;
	int HOUR = 60 * MINUTE;
}
