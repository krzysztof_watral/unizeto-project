# **Pogodynka** #

System składa się z dwóch aplikacji, webowej (spring-boot) oraz graficznej (android). Joby aplikacji webowej/serwerowej w równych odstępach czasu pobierają dane z dwóch zewnętrznych serwisów pogodowych: [wunderground.com](https://espanol.wunderground.com/weather/api) oraz [forecast.io](https://developer.forecast.io/) i zapisują w lokalnej bazie. Pobrane dane prezentowane są w web aplikacji jak i wystawiane prze rest serwisy, które konsumuje aplikacja androidowa.

![webapp.png](https://bitbucket.org/repo/jL8ax7/images/2138728004-webapp.png)

## CZĘŚĆ WEBOWA ##

### Technologie ###
* java 8
* spring-boot (spring core, data, mvc...)
* thymeleaf
* mysql
* maven
* bootstrap

### Konfiguracja ###
* stworzyć bazę w mysql
+ skonfigurować propertiesy w src/main/resources/application.properties
    * do bazy danych:
         * ```spring.datasource.url```
         * ```spring.datasource.username```
         * ```spring.datasource.password```
    * klucze do api (powinny działać te które są ustawione)
         * ```pl.dreamware.weather.api.key.wunderground```
         * ```pl.dreamware.weather.api.key.forecastio```

### Uruchomienie ###
Wykonać poniższą komendę w cmd:

```mvn clean spring-boot:run```

* maven automatycznie stworzy schemat bazy danych
* interfejs webowy będzie dostępny pod adresem http://localhost:8080/

## CZĘŚĆ GRAFICZNA - android ##

Aplikacja napisana w javie 6 pod androida dla urządzeń o wersją api > 17
Testowana na emulatorze z systemem kitkat 4.4.2

![android.png](https://bitbucket.org/repo/jL8ax7/images/909782116-android.png)

### Konfiguracja ###
Endpoint zdefiniowany jest w ```/app/src/main/res/values/strings.xml```
Adres IP ```10.0.2.2``` kieruje do aplikacji serwerowej

### Uruchomienie ###
Aplikację można zbudować przy pomocy gradle wykonując poniższą komendę z cmd

```gradlew build```

Niestety aby uruchomić ją należy:

* zainstalować Android Studio: https://developer.android.com/studio/index.html
* skonfigurować emulator: https://developer.android.com/studio/run/emulator.html

--

*(c) Krzysztof Watral* 